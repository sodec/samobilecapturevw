//
//  SAChipData.swift
//  SAMobileCapture
//
//  Created by Erkan SIRIN on 15.03.2021.
//  Copyright © 2021 Sodec Bilisim Teknolojileri. All rights reserved.
//

import Foundation
import UIKit
import CoreNFC

@objcMembers public class SAChipData : NSObject{
    
    public var activeAuthenticationSupported : Bool
    public var signatureImage : UIImage!
    public var passportImage : UIImage!
    public var activeAuthenticationPassed : Bool
    public var passportDataNotTampered : Bool
    public var documentSigningCertificateVerified : Bool
    public var passportCorrectlySigned : Bool
    public var firstName : NSString
    public var lastName : NSString
    public var nationality : NSString
    public var gender : NSString
    public var dateOfBirth : NSString
    public var documentExpiryDate : NSString
    public var issuingAuthority : NSString
    public var documentNumber : NSString
    public var personalNumber : NSString
    public var documentSubType : NSString
    public var documentType : NSString
    
    
   @objc init(activeAuthenticationSupported: Bool, signatureImage: UIImage!,passportImage: UIImage!,activeAuthenticationPassed: Bool,passportDataNotTampered: Bool,documentSigningCertificateVerified : Bool,passportCorrectlySigned : Bool,firstName : NSString,lastName : NSString,nationality : NSString,gender : NSString,dateOfBirth : NSString,documentExpiryDate : NSString,issuingAuthority : NSString,documentNumber : NSString,personalNumber : NSString,documentSubType : NSString,documentType : NSString) {
        
        self.activeAuthenticationSupported = activeAuthenticationSupported
        self.signatureImage = signatureImage
        self.passportImage = passportImage

        self.activeAuthenticationPassed = activeAuthenticationPassed
        self.passportDataNotTampered = passportDataNotTampered
        self.documentSigningCertificateVerified = documentSigningCertificateVerified
        self.passportCorrectlySigned = passportCorrectlySigned
        self.firstName = firstName
        self.lastName = lastName
        self.nationality = nationality
        self.gender = gender
        self.dateOfBirth = dateOfBirth
        self.documentExpiryDate = documentExpiryDate
        self.issuingAuthority = issuingAuthority
        self.documentNumber = documentNumber
        self.personalNumber = personalNumber
        self.documentSubType = documentSubType
        self.documentType = documentType
        
    }
}
