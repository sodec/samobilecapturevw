//
//  SANFC.swift
//  SAMobileCapture
//
//  Created by Erkan SIRIN on 8.03.2021.
//  Copyright © 2021 Sodec Bilisim Teknolojileri. All rights reserved.
//

import Foundation
import UIKit
import CoreNFC
//delegates function names

@objc public protocol SAChipDelegate{
    
    func didReadChipDone(saChipData: SAChipData)
    func didReadChipDoneWithError(error: NSError)
    func didReadChipCancel()
}

@available(iOS 13, *)
@objcMembers public class SAReadChip : NSObject,SAReadDelegate {
    public func didReadDone(saChipData: SAChipData) {
        print("saChipData : ",saChipData.dateOfBirth)
        self.delegate?.didReadChipDone(saChipData: saChipData)
    }
    
    var delegate :SAChipDelegate?
    let chipReader = NFCWrapper()
    @objc public func readChip(idNumber:String,dateOfBirth:String,expiryDate:String,delegate:SAChipDelegate,masterListURL:URL){
        
        self.delegate = delegate
        
    
        
        chipReader.readNFC(idNumber: idNumber, dateOfBirth: dateOfBirth, expiryDate: expiryDate, certificateFileURL: masterListURL, delegate: self)
        
       
        print("reading requested")
        
    }
    
}



