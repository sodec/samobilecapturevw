/*
* Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Hasan Dertli <hasan.dertli@sodecapps.com>
*
* Sodec Apps Bilisim Teknolojileri
* http://sodecapps.com
* support@sodecapps.com
*/

@interface SABarcodeField : NSObject

- (id)initWithValue:(NSString *)value withPageNumber:(NSUInteger)pageNumber withBoundingBox:(CGRect)boundingBox withTextFont:(UIFont *)textFont withTextColor:(UIColor *)textColor;

@property (strong, nonatomic, readwrite) NSString *barcodeValue;
@property (nonatomic, assign) NSUInteger barcodePageNumber;
@property (nonatomic, assign) CGRect barcodeBoundingBox;
@property (strong, nonatomic, readwrite) UIFont *barcodeTextFont;
@property (strong, nonatomic, readwrite) UIColor *barcodeTextColor;

@end
