//
//  NFCWrapper.swift
//  NFCTest
//
//  Created by Erkan SIRIN on 8.03.2021.
//

import Foundation
import UIKit
import CoreNFC

@objc public protocol SAReadDelegate{
    
    func didReadDone(saChipData: SAChipData)
    
}

@available(iOS 13, *)
@objcMembers public class NFCWrapper : NSObject {
    
    let passportReader = PassportReader()
    
    @objc public func readNFC(idNumber:String,dateOfBirth:String,expiryDate:String,certificateFileURL:URL,delegate:SAReadDelegate){
        print("readNFC")
       
        
        let passportNrChksum = calcCheckSum(idNumber)
        let dateOfBirthChksum = calcCheckSum(dateOfBirth)
        let expiryDateChksum = calcCheckSum(expiryDate)
        
        
        let mrzKey = "\(idNumber)\(passportNrChksum)\(dateOfBirth)\(dateOfBirthChksum)\(expiryDate)\(expiryDateChksum)"

      
       // let masterListURL = Bundle.main.url(forResource: "masterList", withExtension: ".pem")!
        passportReader.setMasterListURL( certificateFileURL )
        print("read starting")
        passportReader.readPassport(mrzKey: mrzKey, customDisplayMessage: { (displayMessage) in
            switch displayMessage {
            
                case .requestPresentPassport:
                    return "Chip özelliği olan dökümanı cihazın üst kısmına tutunuz okuma bitene kadar çekmeyiniz."
                    
                default:
                    print("Chip özelliği olan dökümanı cihazın üst kısmına tutunuz okuma bitene kadar çekmeyiniz.")
                    // Return nil for all other messages so we use the provided default
                    return nil
            }
        }, completed: { (passport, error) in
            if let passport = passport {
                // All good, we got a passport

                DispatchQueue.main.async { [self] in
                   
                    print("documentSigningCertificateVerified : ",passport.documentSigningCertificateVerified)
                    let chipData = SAChipData(activeAuthenticationSupported: passport.activeAuthenticationSupported, signatureImage: passport.signatureImage, passportImage: passport.passportImage, activeAuthenticationPassed: passport.activeAuthenticationPassed, passportDataNotTampered: passport.passportDataNotTampered, documentSigningCertificateVerified: passport.documentSigningCertificateVerified, passportCorrectlySigned: passport.passportCorrectlySigned, firstName: passport.firstName as NSString, lastName: passport.lastName as NSString, nationality: passport.nationality as NSString, gender: passport.gender as NSString, dateOfBirth: passport.dateOfBirth as NSString, documentExpiryDate: passport.documentExpiryDate as NSString, issuingAuthority: passport.issuingAuthority as NSString, documentNumber: passport.documentNumber as NSString, personalNumber: passport.personalNumber as NSString, documentSubType: passport.documentSubType as NSString, documentType: passport.documentType as NSString)
                    
         
                    
                    
                    
                    delegate.didReadDone(saChipData: chipData)
                }

            } else {
                
                print("\(error?.localizedDescription ?? "Unknown error")")
                
            }
        })
    }
    
    
    func calcCheckSum( _ checkString : String ) -> Int {
        let characterDict  = ["0" : "0", "1" : "1", "2" : "2", "3" : "3", "4" : "4", "5" : "5", "6" : "6", "7" : "7", "8" : "8", "9" : "9", "<" : "0", " " : "0", "A" : "10", "B" : "11", "C" : "12", "D" : "13", "E" : "14", "F" : "15", "G" : "16", "H" : "17", "I" : "18", "J" : "19", "K" : "20", "L" : "21", "M" : "22", "N" : "23", "O" : "24", "P" : "25", "Q" : "26", "R" : "27", "S" : "28","T" : "29", "U" : "30", "V" : "31", "W" : "32", "X" : "33", "Y" : "34", "Z" : "35"]
        
        var sum = 0
        var m = 0
        let multipliers : [Int] = [7, 3, 1]
        for c in checkString {
            guard let lookup = characterDict["\(c)"],
                let number = Int(lookup) else { return 0 }
            let product = number * multipliers[m]
            sum += product
            m = (m+1) % 3
        }
        
        return (sum % 10)
    }
    
}






